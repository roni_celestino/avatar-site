import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SobreComponent } from './pages/sobre/sobre.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { ProjetosComponent } from './pages/projetos/projetos.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { SedumComponent } from './pages/sedum/sedum.component';
import { ContatoComponent } from './pages/contato/contato.component';


import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'sobre', component: SobreComponent },
  { path: 'servicos', component: ServicosComponent },
  { path: 'projetos', component: ProjetosComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'sedum', component: SedumComponent },
  { path: 'contato', component: ContatoComponent }
];

@NgModule({
  exports: [RouterModule],
  imports: [ RouterModule.forRoot(routes) ],
})

export class AppRoutingModule { }
