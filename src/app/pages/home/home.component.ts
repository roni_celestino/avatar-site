import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  myStyle: object = {};
  myParams: object = {};
  width = 100;
  height = 100;

  ngOnInit() {
    this.myStyle = {
      'position': 'relative',
      'width': '100%',
      'height': '100%',
      'z-index': 0,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
    };

    this.myParams = {
      particles: {
        number: {
          value: 91,
          density: {
            enable: true,
            value_area: 800
          }
        },
        color: {
          value: '#1c448c'
        },
        shape: {
          type: 'circle',
          stroke: {
            width: 0,
            color: '#3e3eac'
          },
          polygon: {
            nb_sides: 5
          },
        },
        opacity: {
          value: 0.7776548495197786,
          random: true,
          anim: {
            enable: false,
            speed: 1,
            opacity_min: 0.1,
            sync: false
          }
        },
        size: {
          value: 4.5,
          random: true,
          anim: {
            enable: false,
            speed: 40,
            size_min: 0.1,
            sync: false
          }
        },
        line_linked: {
          enable: true,
          distance: 176.3753266952075,
          color: '#151f50',
          opacity: 0.4,
          width: 1.2827296486924182
        },
      }
    };
  }

}
