import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './modules/angular-material.module';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { SwiperModule } from 'angular2-useful-swiper';
import { ParticlesModule } from 'angular-particle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { MenuHeaderComponent } from './component/menu-header/menu-header.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { ProjetosComponent } from './pages/projetos/projetos.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { PortfolioSwiperComponent } from './component/portfolio/portfolio-swiper.component';
import { SedumComponent } from './pages/sedum/sedum.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { SliderSwiperComponent } from './component/slider-swiper/slider-swiper.component';
import { CardServicosComponent } from './component/card-servicos/card-servicos.component';
import { CardPortfolioComponent } from './component/card-portfolio/card-portfolio.component';
import { IbarHomeComponent } from './component/ibar-home/ibar-home.component';
import { SedumHomeComponent } from './component/sedum-home/sedum-home.component';
import { FooterComponent } from './component/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuHeaderComponent,
    SobreComponent,
    ServicosComponent,
    ProjetosComponent,
    PortfolioComponent,
    PortfolioSwiperComponent,
    SedumComponent,
    ContatoComponent,
    SliderSwiperComponent,
    CardServicosComponent,
    CardPortfolioComponent,
    IbarHomeComponent,
    SedumHomeComponent,
    FooterComponent
  ],
  imports: [
    ParticlesModule,
    BrowserModule,
    AngularMaterialModule,
    AppRoutingModule,
    SwiperModule,
    RouterModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgProgressModule.forRoot(),
    NgProgressHttpClientModule,
    NgProgressRouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
