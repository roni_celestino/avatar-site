import { Component, OnInit, ViewChild } from '@angular/core';
import { NgProgress } from '@ngx-progressbar/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  options = {
    minimum: 0.08,
    maximum: 1,
    ease: 'linear',
    speed: 1000,
    trickleSpeed: 300,
    meteor: true,
    spinner: true,
    spinnerPosition: 'right',
    direction: 'leftToRightIncreased',
    color: 'red',
    thick: true
  };

  startedClass = false;
  endedClass = false;
  preventAbuse = false;

  constructor(public progress: NgProgress) {
  }

  ngOnInit() {
    this.progress.started.subscribe(() => {

      this.startedClass = true;
      setTimeout(() => {
        this.startedClass = false;
      }, 1000);
    });

    this.progress.ended.subscribe(() => {

      this.endedClass = true;
      setTimeout(() => {
        this.endedClass = false;
      }, 1000);
    });
  }
}
