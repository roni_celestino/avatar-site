import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio-swiper',
  templateUrl: './portfolio-swiper.component.html',
  styleUrls: ['./portfolio-swiper.component.css']
})
export class PortfolioSwiperComponent implements OnInit {
  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    slidesPerView: 4,
    spaceBetween: 0,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoplay: 3000,
    breakpoints: {
      1024: {
        slidesPerView: 3,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 0,
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 0,
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 0,
      }
    }

  };
  constructor() { }

  ngOnInit() {

  }

}
