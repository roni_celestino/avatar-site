import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.css']
})
export class MenuHeaderComponent implements OnInit {
  constructor() { }
  ngOnInit() {
    window.onscroll = function () { myFunction(); };

    const header = document.getElementById('myHeader');
    const sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        header.classList.add('sticky');
      } else {
        header.classList.remove('sticky');
      }
    }
  }
}
